﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic.FileIO;
using System.Data.SqlClient;

namespace Fight_Promotion_Simulator
{
    class NamesIntoDB
    {

        TextFieldParser parser = new TextFieldParser(@"C:\Users\otpbr\source\Repos\Fight_Promotion_Simulator\us-baby-names\StateNames.csv");
        
        
        public void ParseCSV()
        {
            SqlConnection dbconn = new SqlConnection(Fight_Promotion_Simulator.Properties.Settings.Default.BPDatabaseConnectionString);
            dbconn.Open();

            parser.TextFieldType = FieldType.Delimited;
            parser.SetDelimiters(",");
            int idCounter = 0;

            while (idCounter < 100000)
            {
                int i = 0;
                
                string[] fields = parser.ReadFields();
                foreach(string field in fields)
                {
                    if (fields[3] == "M" && i == 1)
                        {
                        idCounter++;
                        var sqlstatement = "SET IDENTITY_INSERT FirstNamesRaw ON INSERT into FirstNamesRaw (FirstNameID, rawFirstName) values(@idCounter, @field)";
                        using (var command = new SqlCommand(sqlstatement, dbconn))
                        {
                            command.Parameters.AddWithValue("@idCounter", idCounter);
                            command.Parameters.AddWithValue("@field", field);
                            command.ExecuteNonQuery();
                            //Console.WriteLine(field);
                        }
                    }
                    i++;
                    if (i == 6) {i = 0;}
                }
                
            }
            dbconn.Close();
        }
            
                
            
    }
    
    
    
}
