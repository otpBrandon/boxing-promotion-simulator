﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fight_Promotion_Simulator
{
    /// <summary>
    /// This is the main boxer class used in creating boxer objects for
    /// Database entry and stat comparison
    /// </summary>
    class BoxerMain
    {
        // Boxer_Info attributes

        private string firstName = null;
        private string lastName = null;
        private string homeTown = null;
        private int wins = 0;
        private int losses = 0;
        private int draws = 0;
        private int kOs = 0;
        private int beenKOd = 0;

        // Boxer_Behavior attributes

        private string personalityType = null;
        private string style = null;

        // Boxer_Physical attributes

        private decimal height = 0;
        private int weight = 0;
        private decimal reach = 0;
        private string stance = null;

        // Boxer_Skills attributes

        private int jab = 0;
        private int straight = 0;
        private int hook = 0;
        private int uppercut = 0;
        private int dodging = 0;
        private int blocking = 0;
        private int discipline = 0;
        private int reflexes = 0;
        private int chin = 0;
        private int body = 0;
        private int bodyPunching = 0;
        private int recoveryShort = 0;
        private int recoveryLong = 0;

        // Boxer_Stats attributes

        private int strength = 0;
        private int stamina = 0;
        private int handspeed = 0;
        private int footspeed = 0;
        private int power = 0;

        // Boxer_Time attributes

        private string DOB = null;
        private int age = 0;

        // SETTER/GETTERs -------------------------------------------------------
        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public string HomeTown { get => homeTown; set => homeTown = value; }
        public int Wins { get => wins; set => wins = value; }
        public int Losses { get => losses; set => losses = value; }
        public int Draws { get => draws; set => draws = value; }
        public int KOs { get => kOs; set => kOs = value; }
        public int BeenKOd { get => beenKOd; set => beenKOd = value; }
        public string PersonalityType { get => personalityType; set => personalityType = value; }
        public string Style { get => style; set => style = value; }
        public decimal Height { get => height; set => height = value; }
        public int Weight { get => weight; set => weight = value; }
        public decimal Reach { get => reach; set => reach = value; }
        public string Stance { get => stance; set => stance = value; }
        public int Jab { get => jab; set => jab = value; }
        public int Straight { get => straight; set => straight = value; }
        public int Hook { get => hook; set => hook = value; }
        public int Uppercut { get => uppercut; set => uppercut = value; }
        public int Dodging { get => dodging; set => dodging = value; }
        public int Blocking { get => blocking; set => blocking = value; }
        public int Discipline { get => discipline; set => discipline = value; }
        public int Reflexes { get => reflexes; set => reflexes = value; }
        public int Chin { get => chin; set => chin = value; }
        public int Body { get => body; set => body = value; }
        public int BodyPunching { get => bodyPunching; set => bodyPunching = value; }
        public int RecoveryShort { get => recoveryShort; set => recoveryShort = value; }
        public int RecoveryLong { get => recoveryLong; set => recoveryLong = value; }
        public int Strength { get => strength; set => strength = value; }
        public int Stamina { get => stamina; set => stamina = value; }
        public int Handspeed { get => handspeed; set => handspeed = value; }
        public int Footspeed { get => footspeed; set => footspeed = value; }
        public int Power { get => power; set => power = value; }
        public string DOB1 { get => DOB; set => DOB = value; }
        public int Age { get => age; set => age = value; }        
    }
}
