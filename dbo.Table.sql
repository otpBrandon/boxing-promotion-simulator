﻿CREATE TABLE [dbo].[Boxer_Info]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [FirstName] NVARCHAR(50) NOT NULL, 
    [LastName] NVARCHAR(50) NOT NULL, 
    [HomeTown] NVARCHAR(50) NOT NULL, 
    [Wins] INT NOT NULL, 
    [Losses] INT NOT NULL, 
    [Draws] INT NOT NULL, 
    [KOs] INT NOT NULL, 
    [BeenKOd] INT NOT NULL
)
