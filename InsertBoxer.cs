﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Fight_Promotion_Simulator
{
    /// <summary>
    /// This is the class used in filling the initial database
    /// With randomly named boxers and randomized attributes
    /// </summary>
    class InsertBoxer
    {
        // Boxer_Info attributes

        private int boxerId = 0;
        private string firstName = "BLANKFIRST";
        private string lastName = "BLANKLAST";
        private string homeTown = "NOWHERE";
        private int wins = 0;
        private int losses = 0;
        private int draws = 0;
        private int kOs = 0;
        private int beenKOd = 0;
        private string promoter = "FREEAGENT";

        // Boxer_Behavior attributes

        private string personalityType = "NONE";
        private string style = "NOSTYLE";

        // Boxer_Physical attributes

        private decimal height = 0;
        private int weight = 0;
        private decimal reach = 0;
        private string stance = "SITSDOWN";

        // Boxer_Skills attributes

        private int jab = 0;
        private int straight = 0;
        private int hook = 0;
        private int uppercut = 0;
        private int dodging = 0;
        private int blocking = 0;
        private int discipline = 0;
        private int reflexes = 0;
        private int chin = 0;
        private int body = 0;
        private int bodyPunching = 0;
        private int recoveryShort = 0;
        private int recoveryLong = 0;

        // Boxer_Stats attributes

        private int strength = 0;
        private int stamina = 0;
        private int handspeed = 0;
        private int footspeed = 0;
        private int power = 0;

        // Boxer_Time attributes

        private string dOB = "NOTBORNYET";
        private int age = 0;

        // SETTERS --------------------------------------------------
        public int BoxerId { set => boxerId = value; }
        public string FirstName { set => firstName = value; }
        public string LastName { set => lastName = value; }
        public string HomeTown { set => homeTown = value; }
        public int Wins { set => wins = value; }
        public int Losses { set => losses = value; }
        public int Draws { set => draws = value; }
        public int KOs { set => kOs = value; }
        public int BeenKOd { set => beenKOd = value; }
        public string PersonalityType { set => personalityType = value; }
        public string Style { set => style = value; }
        public decimal Height { set => height = value; }
        public int Weight { set => weight = value; }
        public decimal Reach { set => reach = value; }
        public string Stance { set => stance = value; }
        public int Jab { set => jab = value; }
        public int Straight { set => straight = value; }
        public int Hook { set => hook = value; }
        public int Uppercut { set => uppercut = value; }
        public int Dodging { set => dodging = value; }
        public int Blocking { set => blocking = value; }
        public int Discipline { set => discipline = value; }
        public int Reflexes { set => reflexes = value; }
        public int Chin { set => chin = value; }
        public int Body { set => body = value; }
        public int BodyPunching { set => bodyPunching = value; }
        public int RecoveryShort { set => recoveryShort = value; }
        public int RecoveryLong { set => recoveryLong = value; }
        public int Strength { set => strength = value; }
        public int Stamina { set => stamina = value; }
        public int Handspeed { set => handspeed = value; }
        public int Footspeed { set => footspeed = value; }
        public int Power { set => power = value; }
        public string DOB { set => dOB = value; }
        public int Age { set => age = value; }
        public string Promoter { set => promoter = value; }
        

        // Methods -------------------------------------------------------------------------------------
        public void InsertRandomBoxers()
        {
            // Open connection to LocalDB
            SqlConnection dbconn = new SqlConnection(Fight_Promotion_Simulator.Properties.Settings.Default.BPDatabaseConnectionString);
            dbconn.Open();

            // Instantiate random number generator
            Random rand = new Random();
            string[] idList1 = new string[1000];
            string[] idList2 = new string[1000];
            int i = 0;
            int random = 56;
            // Build SQL INSERT statements
            var statementInfo = "SET IDENTITY_INSERT Boxer_Info ON " +
                "INSERT into Boxer_Info (Id, FirstName, LastName, HomeTown, " +
                "Wins, Losses, Draws, KOs, BeenKOd, Promoter, PersonalityType, Style, Height, " +
                "Weight, Reach, Stance, Jab, Straight, Hook, Uppercut, Dodging, Blocking, " +
                "Discipline, Reflexes, Chin, Body, BodyPunching, RecoveryShort, RecoveryLong, " +
                "Strength, Stamina, Handspeed, Footspeed, Power, DOB, Age) " +
                                 "VALUES(@boxerId, @firstName, @lastName, @homeTown, @wins, @losses, @draws, " +
                "@kOs, @beenKOd, @promoter, @personalityType, @style, @height, @weight, @reach, " +
                "@stance, @jab, @straight, @hook, @uppercut, @dodging, @blocking, @discipline, " +
                "@reflexes, @chin, @body, @bodyPunching, @recoveryShort, @recoveryLong, @strength, " +
                "@stamina, @handspeed, @footspeed, @power, @dOB, @age)";

            string randomString = random.ToString();
            var firstNameStatement = "$SELECT rawFirstName FROM FirstNamesRaw WHERE FirstNameId = {randomString}";
                                    //"SELECT rawLastName FROM SurNamesRaw WHERE LastNameId = @random";
            //var lastNameStatement = "SELECT TOP 1 LastName FROM SurNamesRaw ORDER BY NEWID()";

            using (var command1 = new SqlCommand(firstNameStatement, dbconn))
            {
                random = rand.Next(0, 1001);
                //command1.Parameters.AddWithValue("@random", random);
                SqlDataReader reader = command1.ExecuteReader();
                //idList1[i] = reader.GetName(0);
                //idList2[i] = reader.GetName(1);
                Console.WriteLine("\t{0}", reader.GetName(0));
                reader.NextResult();
                //i++;
                         
            }


                    //for (int i = 1; i <= 1000; i++)
                    //{
                    //    boxerId = i;

                    //    using (var command2 = new SqlCommand(firstNameStatement, dbconn))
                    //    {

                    //    }

                    //    // Assign attribute values to database command objects for database entry
                    //    using (var command = new SqlCommand(statementInfo, dbconn))
                    //    {
                    //        command.Parameters.AddWithValue("@boxerId", boxerId);
                    //        command.Parameters.AddWithValue("@firstName", firstName);
                    //        command.Parameters.AddWithValue("@lastName", lastName);
                    //        command.Parameters.AddWithValue("@homeTown", homeTown);
                    //        command.Parameters.AddWithValue("@wins", wins);
                    //        command.Parameters.AddWithValue("@losses", losses);
                    //        command.Parameters.AddWithValue("@draws", draws);
                    //        command.Parameters.AddWithValue("@kOs", kOs);
                    //        command.Parameters.AddWithValue("@beenKOd", beenKOd);
                    //        command.Parameters.AddWithValue("promoter", promoter);
                    //        command.Parameters.AddWithValue("@personalityType", personalityType);
                    //        command.Parameters.AddWithValue("@style", style);
                    //        command.Parameters.AddWithValue("@height", height);
                    //        command.Parameters.AddWithValue("@weight", weight);
                    //        command.Parameters.AddWithValue("@reach", reach);
                    //        command.Parameters.AddWithValue("@stance", stance);
                    //        command.Parameters.AddWithValue("@jab", jab);
                    //        command.Parameters.AddWithValue("@straight", straight);
                    //        command.Parameters.AddWithValue("@hook", hook);
                    //        command.Parameters.AddWithValue("@uppercut", uppercut);
                    //        command.Parameters.AddWithValue("@dodging", dodging);
                    //        command.Parameters.AddWithValue("@blocking", blocking);
                    //        command.Parameters.AddWithValue("@discipline", discipline);
                    //        command.Parameters.AddWithValue("@reflexes", reflexes);
                    //        command.Parameters.AddWithValue("@chin", chin);
                    //        command.Parameters.AddWithValue("@body", body);
                    //        command.Parameters.AddWithValue("@bodyPunching", bodyPunching);
                    //        command.Parameters.AddWithValue("@recoveryShort", recoveryShort);
                    //        command.Parameters.AddWithValue("@recoveryLong", recoveryLong);
                    //        command.Parameters.AddWithValue("@strength", strength);
                    //        command.Parameters.AddWithValue("@stamina", stamina);
                    //        command.Parameters.AddWithValue("@handspeed", handspeed);
                    //        command.Parameters.AddWithValue("@footspeed", footspeed);
                    //        command.Parameters.AddWithValue("@power", power);
                    //        command.Parameters.AddWithValue("@DOB", dOB);
                    //        command.Parameters.AddWithValue("@age", age);

                    //        command.ExecuteNonQuery();
                    //    }
                    //}
                
            

            
            

            MessageBox.Show("Finished.");
            
            dbconn.Close();
        }
    }

    
}
