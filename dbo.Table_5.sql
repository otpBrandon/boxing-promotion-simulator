﻿CREATE TABLE [dbo].[Boxer_Skills]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [BoxerId] INT NOT NULL, 
    [Jab] INT NOT NULL, 
    [Straight] INT NOT NULL, 
    [Hook] INT NOT NULL, 
    [Uppercut] INT NOT NULL, 
    [Dodging] INT NOT NULL, 
    [Blocking] INT NOT NULL, 
    [Discipline] INT NOT NULL, 
    [Reflexes] INT NOT NULL, 
    [Chin] INT NOT NULL, 
    [Body] INT NOT NULL, 
    [BodyPunching] INT NOT NULL, 
    [RecoveryShort] INT NOT NULL, 
    [RecoveryLong] INT NOT NULL, 
    CONSTRAINT [FK_Boxer_skills_Boxer_info] FOREIGN KEY ([BoxerId]) REFERENCES [Boxer_Info] ([Id])
)
