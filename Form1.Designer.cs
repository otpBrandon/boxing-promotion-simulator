﻿namespace Fight_Promotion_Simulator
{
    partial class frmMainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbl_JBPS_Main = new System.Windows.Forms.Label();
            this.btnCreate_Start = new System.Windows.Forms.Button();
            this.lblPromoName = new System.Windows.Forms.Label();
            this.txtPromoterName = new System.Windows.Forms.TextBox();
            this.lblWarChest = new System.Windows.Forms.Label();
            this.btnDeleteDB = new System.Windows.Forms.Button();
            this.btnFillNamesDB = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lbl_JBPS_Main
            // 
            this.lbl_JBPS_Main.AutoSize = true;
            this.lbl_JBPS_Main.Font = new System.Drawing.Font("Birch Std", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_JBPS_Main.ForeColor = System.Drawing.Color.DarkRed;
            this.lbl_JBPS_Main.Location = new System.Drawing.Point(109, 31);
            this.lbl_JBPS_Main.Name = "lbl_JBPS_Main";
            this.lbl_JBPS_Main.Size = new System.Drawing.Size(344, 56);
            this.lbl_JBPS_Main.TabIndex = 0;
            this.lbl_JBPS_Main.Text = "Boxing Promotion Simulator";
            this.lbl_JBPS_Main.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnCreate_Start
            // 
            this.btnCreate_Start.BackColor = System.Drawing.Color.Black;
            this.btnCreate_Start.Font = new System.Drawing.Font("Hobo Std", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreate_Start.ForeColor = System.Drawing.Color.DarkRed;
            this.btnCreate_Start.Location = new System.Drawing.Point(129, 215);
            this.btnCreate_Start.Name = "btnCreate_Start";
            this.btnCreate_Start.Size = new System.Drawing.Size(312, 79);
            this.btnCreate_Start.TabIndex = 1;
            this.btnCreate_Start.Text = "Create Your Promoter and Start";
            this.btnCreate_Start.UseVisualStyleBackColor = false;
            this.btnCreate_Start.Click += new System.EventHandler(this.BtnCreate_Start_Click);
            // 
            // lblPromoName
            // 
            this.lblPromoName.AutoSize = true;
            this.lblPromoName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPromoName.ForeColor = System.Drawing.Color.DarkRed;
            this.lblPromoName.Location = new System.Drawing.Point(145, 119);
            this.lblPromoName.Name = "lblPromoName";
            this.lblPromoName.Size = new System.Drawing.Size(197, 16);
            this.lblPromoName.TabIndex = 2;
            this.lblPromoName.Text = "Enter your promotion name:";
            // 
            // txtPromoterName
            // 
            this.txtPromoterName.Location = new System.Drawing.Point(148, 138);
            this.txtPromoterName.Name = "txtPromoterName";
            this.txtPromoterName.Size = new System.Drawing.Size(284, 20);
            this.txtPromoterName.TabIndex = 6;
            // 
            // lblWarChest
            // 
            this.lblWarChest.AutoSize = true;
            this.lblWarChest.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWarChest.ForeColor = System.Drawing.Color.DarkRed;
            this.lblWarChest.Location = new System.Drawing.Point(179, 166);
            this.lblWarChest.Name = "lblWarChest";
            this.lblWarChest.Size = new System.Drawing.Size(206, 16);
            this.lblWarChest.TabIndex = 7;
            this.lblWarChest.Text = "Starting war chest:   $200,000";
            // 
            // btnDeleteDB
            // 
            this.btnDeleteDB.Location = new System.Drawing.Point(458, 225);
            this.btnDeleteDB.Name = "btnDeleteDB";
            this.btnDeleteDB.Size = new System.Drawing.Size(98, 59);
            this.btnDeleteDB.TabIndex = 8;
            this.btnDeleteDB.Text = "Clear DB";
            this.btnDeleteDB.UseVisualStyleBackColor = true;
            this.btnDeleteDB.Click += new System.EventHandler(this.BtnDeleteDB_Click);
            // 
            // btnFillNamesDB
            // 
            this.btnFillNamesDB.Location = new System.Drawing.Point(458, 138);
            this.btnFillNamesDB.Name = "btnFillNamesDB";
            this.btnFillNamesDB.Size = new System.Drawing.Size(98, 59);
            this.btnFillNamesDB.TabIndex = 9;
            this.btnFillNamesDB.Text = "Fill Names DB";
            this.btnFillNamesDB.UseVisualStyleBackColor = true;
            this.btnFillNamesDB.Click += new System.EventHandler(this.BtnFillNamesDB_Click);
            // 
            // frmMainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(568, 326);
            this.Controls.Add(this.btnFillNamesDB);
            this.Controls.Add(this.btnDeleteDB);
            this.Controls.Add(this.lblWarChest);
            this.Controls.Add(this.txtPromoterName);
            this.Controls.Add(this.lblPromoName);
            this.Controls.Add(this.btnCreate_Start);
            this.Controls.Add(this.lbl_JBPS_Main);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "frmMainMenu";
            this.Text = "Boxing Promoter Simulator Main Menu";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_JBPS_Main;
        private System.Windows.Forms.Button btnCreate_Start;
        private System.Windows.Forms.Label lblPromoName;
        private System.Windows.Forms.TextBox txtPromoterName;
        private System.Windows.Forms.Label lblWarChest;
        private System.Windows.Forms.Button btnDeleteDB;
        private System.Windows.Forms.Button btnFillNamesDB;
    }
}

