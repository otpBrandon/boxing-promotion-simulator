﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Fight_Promotion_Simulator
{
    class ClearDB
    {
        public void DeleteData()
        {
            SqlConnection dbconn = new SqlConnection(Fight_Promotion_Simulator.Properties.Settings.Default.BPDatabaseConnectionString);

            dbconn.Open();
            var statement = "DELETE Boxer_Info WHERE Id > 0";
            SqlDataAdapter adapter = new SqlDataAdapter();

            adapter.DeleteCommand = new SqlCommand(statement, dbconn);
            adapter.DeleteCommand.ExecuteNonQuery();
            
            dbconn.Close();
            
        }
    }
}
